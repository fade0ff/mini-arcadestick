A miniature arcade stick with USB and Bluetooth

See http://lemmini.de/Mini-Arcadestick/Mini-Arcadestick.html

---------------------------------------------------------------
Copyright 2019 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.
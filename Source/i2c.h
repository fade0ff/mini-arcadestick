/** I2C library
	Communication via I2C peripheral
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef I2C_H
#define I2C_H

#include "i2c_cnf.h"

#define LPC_I2C_Type LPC_I2C_T


/* I2C Configuration register  */

#define I2C_CFG_MSTEN                (1UL << 0)        ///! master enable/disable bit
#define I2C_CFG_SLVEN                (1UL << 1)        ///! slave enable/disable bit
#define I2C_CFG_MONEN                (1UL << 2)        ///! monitor enable/disable bit
#define I2C_CFG_TIMEOUTEN            (1UL << 3)        ///! timeout enable/disable bit
#define I2C_CFG_MONCLKSTR            (1UL << 4)        ///! monitor clock stretching bit

/** Get control register for physical I2C device
	@param x pointer to I2C peripheral
	@return CFG register
*/
#define I2C_GetConfig(x)    ((x)->CFG )

/** Set bits in control register for physical I2C device.
	@param x pointer to I2C peripheral
	@param y bitmask to set in CFG (construct from I2C_CFG_xxx macros)
*/
#define I2C_SetConfig(x,y)  {(x)->CFG = (u32)((y)&0x1f);}


/* I2C Status register  */
#define I2C_STAT_MSTPENDING          (1UL << 0)        ///! master pending status bit
#define I2C_STAT_MSTSTATE_MASK       (7UL << 1)        ///! master state code
#define I2C_STAT_MSTSTATE_IDLE       (0UL << 1)        ///! master state: idle
#define I2C_STAT_MSTSTATE_RXREADY    (1UL << 1)        ///! master state: receive ready
#define I2C_STAT_MSTSTATE_TXREADY    (2UL << 1)        ///! master state: transmit ready
#define I2C_STAT_MSTSTATE_NACKADR    (3UL << 1)        ///! master state: nack by slave on address
#define I2C_STAT_MSTSTATE_NACKDAT    (4UL << 1)        ///! master state: nack by slave on data
#define I2C_STAT_MSTRARBLOSS         (1UL << 4)        ///! master arbitration loss bit (write 1 to clear)
#define I2C_STAT_MSTSTSTPERR         (1UL << 6)        ///! master start stop error bit (write 1 to clear)
#define I2C_STAT_SLVPENDING          (1UL << 8)        ///! slave pending status bit
#define I2C_STAT_SLVSTATE_MASK       (3UL << 9)        ///! slave state code
#define I2C_STAT_SLVSTATE_ADDR       (0UL << 9)        ///! slave state: slave address received
#define I2C_STAT_SLVSTATE_RXREADY    (1UL << 9)        ///! slave state: receive ready
#define I2C_STAT_SLVSTATE_TXREADY    (2UL << 9)        ///! slave state: transmit ready
#define I2C_STAT_SLVNOTSTR           (1UL << 11)       ///! slave not stretching clock bit
#define I2C_STAT_SLVIDX              (3UL << 12)       ///! slave address index
#define I2C_STAT_SLVSEL              (1UL << 14)       ///! slave selected bit
#define I2C_STAT_SLVDESEL            (1UL << 15)       ///! slave deselect bit (write 1 to clear)
#define I2C_STAT_MONRDY              (1UL << 16)       ///! monitor ready bit
#define I2C_STAT_MONOV               (1UL << 17)       ///! monitor overflow flag (write 1 to clear)
#define I2C_STAT_MONACTIVE           (1UL << 18)       ///! monitor active flag
#define I2C_STAT_MONIDLE             (1UL << 19)       ///! monitor idle flag  (write 1 to clear)
#define I2C_STAT_EVENTTIMEOUT        (1UL << 24)       ///! event timeout interrupt flag  (write 1 to clear)
#define I2C_STAT_SCLTIMEOUT          (1UL << 25)       ///! scl timeout interrupt flag  (write 1 to clear)

/** Get status register for physical I2C device
	@param x pointer to I2C peripheral
	@return CFG register
*/
#define I2C_GetStatus(x)    ((x)->STAT )

/** Clrs bits in status register for physical I2C device by setting them.
	@param x pointer to I2C peripheral
	@param y bitmask to set in CFG (construct from I2C_STAT_xxx macros)
*/
#define I2C_ClrStatus(x,y)  {(x)->STAT = (u32)((y)&0x30a8050);}

/** Get slave index from status register
	@param status I2C status register read through I2C_GetStatus
	@return slave index (0..3)
*/
#define I2C_GetSlaveIndex(status) (((status)>>12)&3)


/* I2C Interrupt enable set register */
#define I2C_INTENSET_MSTPENDING      (1UL << 0)        ///! master pending interrupt enable
#define I2C_INTENSET_MSTRARBLOSS     (1UL << 4)        ///! master arbitration loss interrupt enable
#define I2C_INTENSET_MSTSTSTPERR     (1UL << 6)        ///! master start stop error interrupt enable
#define I2C_INTENSET_SLVPENDING      (1UL << 8)        ///! slave pending interrupt enable bit
#define I2C_INTENSET_SLVNOTSTR       (1UL << 11)       ///! slave not stretching clock interrupt enable
#define I2C_INTENSET_SLVDESEL        (1UL << 15)       ///! slave deselect interrupt enable
#define I2C_INTENSET_MONRDY          (1UL << 16)       ///! monitor ready interrupt enable
#define I2C_INTENSET_MONOV           (1UL << 17)       ///! monitor overflow interrupt enable
#define I2C_INTENSET_MONIDLE         (1UL << 19)       ///! monitor idle interrupt enable
#define I2C_INTENSET_EVENTTIMEOUT    (1UL << 24)       ///! event timeout interrupt enable
#define I2C_INTENSET_SCLTIMEOUT      (1UL << 25)       ///! scl timeout interrupt enable

/** Get I2C interrupt mask set/read register.
	@param x pointer to I2C peripheral
	@return interrupt enable register INTENSET
 */
#define I2C_GetIntEnable(x)   ( (x)->INTENSET )

/** Set I2C interrupt mask set/read register.
    Write ones to enable a certain interrupt.
	@param x pointer to I2C peripheral
	@param y value to write to interrupt enable register (construct from I2C_INT_xxx macros)
 */
#define I2C_SetIntEnable(x,y) { (x)->INTENSET = (y) & 0x30b8951;}


/* I2C Interrupt enable clear register */

/** Set I2C interrupt clear register.
    Write ones to disable a certain interrupt.
	@param x pointer to I2C peripheral
	@param y value to write to interrupt enable register (construct from I2C_INT_xxx macros)
 */
#define I2C_ClrIntEnable(x,y) { (x)->INTENCLR = (y) & 0x30b8951;}


/* I2C Timeout value register */

/** Get I2C timeout register.
	@param x pointer to I2C peripheral
	@return timeout register TIMEOUT
 */
#define I2C_GetTimeout(x)   ( (x)->TIMEOUT )

/** Set I2C timeout register.
	@param x pointer to I2C peripheral
	@param y value in increments of 16 I2C clock (0x10..0xffff)
 */
#define I2C_SetTimeout(x,y) { (x)->TIMEOUT = (u32)(((y) & 0xFFF0) | 0x0f);}


/* I2C divider register */

/** Get I2C divider register.
	@param x pointer to I2C peripheral
	@return divider register DIVVAL value (1..65536)
 */
#define I2C_GetDivider(x)   ( (((x)->CLKDIV)&0xffff) + 1  )

/** Set I2C Clock Divider Register
	@param x pointer to I2C peripheral
	@param div divider value 1..65536
 */
#define I2C_SetDivider(x,div) { (x)->CLKDIV = (u32)(((div)-1)&0xffff);}


/* I2C Interrupt status register */
#define I2C_INTSTAT_MSTPENDING       (1UL << 0)        ///! master pending interrupt status bit
#define I2C_INTSTAT_MSTRARBLOSS      (1UL << 4)        ///! master arbitration loss interrupt status bit
#define I2C_INTSTAT_MSTSTSTPERR      (1UL << 6)        ///! master start stop error interrupt status bit
#define I2C_INTSTAT_SLVPENDING       (1UL << 8)        ///! slave pending interrupt status bit
#define I2C_INTSTAT_SLVNOTSTR        (1UL << 11)       ///! slave not stretching clock interrupt status bit
#define I2C_INTSTAT_SLVDESEL         (1UL << 15)       ///! slave deselect interrupt status bit
#define I2C_INTSTAT_MONRDY           (1UL << 16)       ///! monitor ready interrupt status bit
#define I2C_INTSTAT_MONOV            (1UL << 17)       ///! monitor overflow interrupt status bit
#define I2C_INTSTAT_MONIDLE          (1UL << 19)       ///! monitor idle interrupt status bit
#define I2C_INTSTAT_EVENTTIMEOUT     (1UL << 24)       ///! event timeout interrupt status bit
#define I2C_INTSTAT_SCLTIMEOUT       (1UL << 25)       ///! scl timeout interrupt status bit

/** Get I2C interrupt status register.
	@param x pointer to I2C peripheral
	@return interrupt status register INTSTAT
 */
#define I2C_GetIntStatus(x)   ( (x)->INTSTAT )

/* I2C Master control register */
#define I2C_MSTCTL_MSTCONTINUE       (1UL << 0)        ///! master continue bit
#define I2C_MSTCTL_MSTSTART          (1UL << 1)        ///! master start control bit
#define I2C_MSTCTL_MSTSTOP           (1UL << 2)        ///! master stop control bit
#define I2C_MSTCTL_MSTDMA            (1UL << 3)        ///! master dma enable bit

/** Get I2C master control register.
	@param x pointer to I2C peripheral
	@return master control register
 */
#define I2C_GetMasterControl(x)   ( (x)->MSTCTL )

/** Set I2C master control register
	@param x pointer to I2C peripheral
	@param y value to write to master control register (construct from I2C_MSTCTL_xxx macros)
 */
#define I2C_SetMasterControl(x,y) { (x)->MSTCTL = (u32)((y)&15);}

/* I2C Master time register */
#define I2C_MSTTIME_MSTSCLLOW_MASK   (7UL << 0)        ///! master scl low time field
#define I2C_MSTTIME_MSTSCLHIGH_MASK  (7UL << 4)        ///! master scl high time field

/** Get I2C master time register.
	@param x pointer to I2C peripheral
	@return master time register
 */
#define I2C_GetMasterTime(x)   ( (x)->MSTTIME )

/** Set I2C master time register
	@param x pointer to I2C peripheral
	@param lo  scl low time in I2C clock pre-divider ticks (0..7, 2 always added by HW)
	@param hi  scl high time in I2C clock pre-divider ticks (0..7, 2 always added by HW)
 */
#define I2C_SetMasterTime(x,lo,hi) { (x)->MSTTIME = (u32)((((hi)&7)<<4)|((lo)&7)) ;}


/* I2C Master data register */

/** Write byte to data register for physical I2C device
	@param x pointer to I2C peripheral
	@param d data byte to write
*/
#define I2C_SetData(x, d)    {(x)->MSTDAT = (u32)((d)&0xff);}

/** Read byte from data register of physical I2C device
	@param x pointer to I2C peripheral
	@return d data byte
*/
#define I2C_GetData(x)       ((u8)((x)->MSTDAT))


/* I2C Slave control register */
#define I2C_SLVCTL_SLVCONTINUE       (1UL << 0)        ///! slave continue bit
#define I2C_SLVCTL_SLVNACK           (1UL << 1)        ///! slave nack bit
#define I2C_SLVCTL_SLVDMA            (1UL << 3)        ///! slave dma enable bit

/** Get I2C slave control register.
	@param x pointer to I2C peripheral
	@return slave control register
 */
#define I2C_GetSlaveControl(x)   ( (x)->SLVCTL )

/** Set I2C slave control register
	@param x pointer to I2C peripheral
	@param y value to write to slave control register (construct from I2C_MSTCTL_xxx macros)
 */
#define I2C_SetSlaveControl(x,y) { (x)->SLVCTL = (u32)((y)&11);}

/* I2C Slave data register*/
/** Write byte to slave data register for physical I2C device
	@param x pointer to I2C peripheral
	@param d data byte to write
*/
#define I2C_SetSlaveData(x, d)    {(x)->SLVDAT = (u32)((d)&0xff);}

/** Read byte from slave data register of physical I2C device
	@param x pointer to I2C peripheral
	@return d data byte
*/
#define I2C_GetSlaveData(x)       ((u8)((x)->SLVDAT))

/* I2C Slave Address register */
#define I2C_SLVADR_SADISABLE         (1UL << 0)        ///! slave address n disable bit
#define I2C_SLVADR_SLVADR_MASK       (127UL << 1)      ///! slave address field

/** Read slave address register of physical I2C device
	@param x pointer to I2C peripheral
	@return value from slave
*/
#define I2C_GetSlaveAddr(x)       ((u8)((x)->SLVADR))

/** Write slave address register for physical I2C device
	@param x pointer to I2C peripheral
	@param d data write
*/
#define I2C_SetSlaveAddr(x, d)    {(x)->SLVADR = (u32)((d)&0xff);}

/** Enable/disable slave address.
	@param x pointer to I2C peripheral
	@param a slave address to enable/disable
	@param e 1: enable, 0: disbale
*/
#define I2C_EnableSlaveAddr(x, a, e)    {(x)->SLVADR = (u32)( (((a)&127)<<1) | ( ((e)!=0)?1:0) );}

/* I2C Slave address qualifier 0 register */
#define I2C_SLVQUAL_QUALMODE0        (1UL << 0)        ///! slave qualifier mode enable bit
#define I2C_SLVQUAL_SLVQUAL0         (127UL << 1)      ///! slave qualifier address for address 0

/** Read slave address qualifier 0 register of physical I2C device
	@param x pointer to I2C peripheral
	@return value from slave
*/
#define I2C_GetSlaveAddrQ0(x)       ((u8)((x)->SLVADR))

/** Write address qualifier 0 register for physical I2C device
	@param x pointer to I2C peripheral
	@param d data write
*/
#define I2C_SetSlaveAddrQ0(x, d)    {(x)->SLVADR = (u32)((d)&0xff);}

/** Enable/disable slave address qualifier 0.
	@param x pointer to I2C peripheral
	@param a slave address to enable/disable
	@param e 1: enable, 0: disbale
*/
#define I2C_EnableSlaveAddrQ0(x, a, e)    {(x)->SLVADR = (u32)( (((a)&127)<<1) | ( ((e)!=0)?1:0) );}

/* I2C Monitor data register */
#define I2C_MONRXDAT_DATA             (255UL << 0)     ///! monitor function receive data field
#define I2C_MONRXDAT_MONSTART         (1UL << 8)       ///! monitor received start bit
#define I2C_MONRXDAT_MONRESTART       (1UL << 9)       ///! monitor received repeated start bit
#define I2C_MONRXDAT_MONNACK          (1UL << 10)      ///! monitor received nack bit

/** Get value of monitor data register for physical I2C device
	@param x pointer to I2C peripheral
	@return value of MONRXDAT register
*/
#define I2C_GetMonitorData(x)     ((x)->MONRXDAT & 0xF8 )


/* Transfer states - defined as bitfield to serve as faults */
#define I2C_STATE_INACTIVE         0UL       ///! Inactive
#define I2C_STATE_ADR_WRITE        (1UL<<1)  ///! Send address to slave
#define I2C_STATE_DATA_WRITE       (1UL<<2)  ///! Data sent to slave
#define I2C_STATE_DATA_READ        (1UL<<3)  ///! Data data from slave
#define I2C_STATE_REPEAT_START     (1UL<<4)  ///! Repeat start to switch from writing to reading


#define I2C_STATE_NO_ACK_FAULT     (1UL<<9)  ///!  SLA+R has been transmitted, NACK has been received
#define I2C_STATE_ARB_LOST         (1UL<<10) ///!  Arbitration lost
#define I2C_STATE_START_STOP_ERR   (1UL<<10) ///!  Master start stop error

/* Transfer types */
#define I2C_TYPE_MST_READ          0         ///! Master transfer, random read via 16bit address
#define I2C_TYPE_MST_WRITE         1         ///! Master transfer, random write via 16bit address

#define I2C_SLAVE_ADR_M_TX_WRITE   0         ///! Write slave address
#define I2C_SLAVE_ADR_M_TX_READ    1         ///! Read slave address

/* Transfer result values */
#define I2C_MST_TRANSFER_FAILED    0         ///! Master transfer failed
#define I2C_MST_TRANSFER_OK        1         ///! Master transfer ok
#define I2C_MST_TRANSFER_QUEUED    2         ///! Master transfer queued

/* Address sizes */
#define I2C_ADR_SIZE_NONE          0         ///! No address given
#define I2C_ADR_SIZE_8BIT          1         ///! One byte address
#define I2C_ADR_SIZE_16BIT         2         ///! Two bytes address
#define I2C_ADR_SIZE_24BIT         3         ///! Three bytes address
#define I2C_ADR_SIZE_32BIT         4         ///! Four bytes address

#define I2C_DC_SCALE               0x8000    ///! 100% duty cycle for high/low time configuration

/* Type definitions */

typedef void (*i2c_callback_t)(void);        ///! Function pointer for end of transfer handler

/** Device RAM structure */
typedef struct {
	u16 state;                               ///! Current device state
	u16 transfer_count;                      ///! Number of bytes transferred
	u8  active_ch;                           ///! Channel actually served
	u8  queue_bottom_idx;                    ///! Index to free entry in queue
	u8  queue_entries;                       ///! Number of entries currently in queue
	u8  retry_ctr;                           ///! Counter for retries on error
	u8  overflow;                            ///! Queue overflow flag
} i2c_device_t;

/** Channel RAM structure */
typedef struct {
	u16 divider;                             ///! Divider
	u16 faults;                              ///! Bitfield containing faults for this channel
	u8  sclh;                                ///! SCL high time period count
	u8  scll;                                ///! SCL low time period count
	u8  pending_count;                       ///! Number of pending transfers for this channel
} i2c_channel_t;

/** Queue RAM structure */
typedef struct {
	i2c_callback_t callback;                 ///! Pointer to callback function if transfer is finished
	u8* buffer_ptr;                          ///! Pointer to source/target buffer
	u32 address;                             ///! 8bit to 32bit address for random read/write
	u16 len;                                 ///! Length of data transfer in bytes
	u8  slave_address;                       ///! Store slave address in queue (for exotic ICs that need to change the slave adddress)
	u8  ch_id;                               ///! Reference to configuration channel
	u8  type;                                ///! Type of transfer [I2C_TYPE_MST_xxx]
} i2c_queue_t;

/** Timing configuration structure */
typedef struct {
	u32 baudrate;                            ///! baudrate/frequency in Hz
	u16 duty_cycle;                          ///! duty cycle in 100% == I2C_DC_SCALE (default 0x8000) resolution
} i2c_timing_cfg_t;

/** Channel configuration structure */
typedef struct {
	u8  device_id;                           ///! virtual device index
	u8  slave_address;                       ///! 7bit slave address
	u8  adr_size;                            ///! size of address 0..4 bytes
	u8  timing_profile_idx;                  ///! index of timing profile
	u8  retries;                             ///! number of retries in case of faults
} i2c_channel_cfg_t;

/* Exports */
extern const LPC_I2C_Type *i2c_device_cfg[I2C_VIRTUAL_DEVICE_NUM];
extern const i2c_channel_cfg_t i2c_channel_cfg[];
extern const i2c_timing_cfg_t i2c_timing_profile_cfg[I2C_TIMING_PROFILES_NUM];

/* Prototypes */
extern void I2C_Init(void);
extern   u8 I2C_MasterTransferX(u8 ch_id, u8 slave_address, u8 type, u32 address, u8* buffer_ptr, u16 len, i2c_callback_t callback);
extern   u8 I2C_MasterTransfer(u8 ch_id, u8 type, u32 address, u8* buffer_ptr, u16 len, i2c_callback_t i2c_callback_t);
extern  u16 I2C_GetFaults(u8 ch_id);
extern   u8 I2C_QueueEntriesAvailable(u8 ch_id);
extern   u8 I2C_GetChannelPendingCount(u8 ch_id);
extern   u8 I2C_GetDeviceID(u8 ch_id);
extern LPC_I2C_Type* I2C_GetDevice(u8 ch_id);


#endif

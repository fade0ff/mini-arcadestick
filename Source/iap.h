/** In-Application Programming
	Calling of ROM functions
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef IAP_H
#define IAP_H

#define IAP_LOCATION 0x03000205UL

typedef void (*IAP) (u32 command[], u32 result[]);

#define IAP_CMD_PREPARE_SECTOR_FOR_WRITE  50
#define IAP_CMD_COPY_RAM_TO_FLASH         51
#define IAP_CMD_ERASE_SECTOR              52
#define IAP_CMD_BLANK_CHECK               53
#define IAP_CMD_READ_PART_ID              54
#define IAP_CMD_READ_BOOT_CODE_VERSION    55
#define IAP_CMD_COMPARE                   56
#define IAP_CMD_REINVOKE_ISP              57
#define IAP_CMD_READ_UID                  58
#define IAP_CMD_ERASE_PAGE                59
#define IAP_CMD_EEPROM_WRITE              61
#define IAP_CMD_EEPROM_READ               62

#endif

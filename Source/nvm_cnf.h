/** NVM library - Configuration
	Configuration of non volatile memory - internal EEPROM
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVM_CNF_H
#define NVM_CNF_H

#define NVM_SIZE 4096              /* number of bytes in NVRAM */


#endif

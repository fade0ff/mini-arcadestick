/** NVM library - Configuration
	Configuration of non volatile memory - I2C EEPROM (AT24C256/AT24C128)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "nvmi2c.h"

/* Project specific NVRAM variables */

/* Project specific NVRAM functions */

/** NVM library
	Non volatile memory implemented via an I2C EEPROM
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVMI2C_H
#define NVMI2C_H

#include "nvmi2c_cnf.h"

#define NVM_I2C_SLAVE_ADDRESS 0x50 ///! I2C slave address for EEPROM

/* Prototypes */
extern void NVMI2C_Init(void);
extern u8 NVMI2C_Write(u16 adr, u8 *data, u16 len);
extern u8 NVMI2C_Read(u16 adr, u8 *data, u16 len);
extern void NVMI2C_WaitForCompletion(void);

#endif

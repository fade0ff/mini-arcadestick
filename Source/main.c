/** USB Joystick
	A flexible USB HID-Joystick implementation

	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#include "global.h"
#include <string.h>
#include <stdlib.h>
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "uart.h"
#include "i2c.h"
#include "sct.h"
#include "systime.h"
#include "systick.h"
#include "systime.h"
#include "romapi.h"
#include "adc.h"
#include "cmsis/app_usbd_cfg.h"
#include "cmsis/hid_generic.h"
#include "segger/SEGGER_RTT.h"


//#define RTT_LOG 1
#define RTT_BT  0

#define VOLTAGE_LOW  2110 // 3.4V : 3.4*(1<<12)/(3.3*2) = 2110
#define VOLTAGE_HIGH 2668 // 4.3V : 4.3*(1<<12)/(3.3*2) = 2668.61

#define PIN_DEBOUNCE_COUNT 10
#define NUMBER_OF_AXES     2
#define NUMBER_OF_BUTTONS  8

static USBD_HANDLE_T g_hUsb;
const  USBD_API_T *g_pUsbApi;


/// general channel configuration options
#define CNF_INVERT   (1UL<<7)
#define CNF_DISABLE  0
#define CNF_ENABLE   (1UL<<0)

/// Joystick mode channel configuration options
#define CNF_DIGITAL (1UL<<2)
#define CNF_ANALOG  0

////////////////////////////////////////////////////////////////////////////////////////

// index in debounce array
#define BUTTON_1 0
#define BUTTON_2 1
#define BUTTON_3 2
#define BUTTON_4 3
#define BUTTON_5 4
#define BUTTON_6 5
#define BUTTON_7 6
#define BUTTON_8 7
#define UP       8
#define DOWN     9
#define LEFT     10
#define RIGHT    11

////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	u8 resourceL;
	u8 resourceR;
	u8 mode;
} axis_config_t;

typedef struct {
	u8 resource;
	u8 mode;
} button_config_t ;

typedef struct  {
	axis_config_t   axis[NUMBER_OF_AXES];
	button_config_t button[NUMBER_OF_BUTTONS];
} config_t;

config_t config = {
	.axis = {
		{ LEFT,      RIGHT,   CNF_ENABLE | CNF_DIGITAL}, //X
		{ UP,        DOWN,    CNF_ENABLE | CNF_DIGITAL}, //Y
	},
	.button = {
		{ BUTTON_1,          CNF_ENABLE },
		{ BUTTON_2,          CNF_ENABLE },
		{ BUTTON_3,          CNF_ENABLE },
		{ BUTTON_4,          CNF_ENABLE },
		{ BUTTON_5,          CNF_ENABLE },
		{ BUTTON_6,          CNF_ENABLE },
		{ BUTTON_7,          CNF_ENABLE },
		{ BUTTON_8,          CNF_ENABLE }
	}
};

u8 hid_output_report[HID_OUT_REPORT_SIZE];

u16 axes_filtered[NUMBER_OF_AXES];

// input debouncing
typedef struct {
	u8 pin;          ///! pin number
	s8 ctr;          ///! debounce counter
	u8 state;        ///! debounce state: 0: debounced, 1: debouncing ongoing
	volatile u8 lvl; ///! debounced logic level
	u8 inact_lvl;    ///! inactive level: 0: low, 1: high
} pin_debounce_t;

/** debounce structure for the input pins */
pin_debounce_t pins[] = {
	/* pin, counter, state, level, inactive level */
	{PIN_BUTTON_1, 0,0,0,1},
	{PIN_BUTTON_2, 0,0,0,1},
	{PIN_BUTTON_3, 0,0,0,1},
	{PIN_BUTTON_4, 0,0,0,1},
	{PIN_BUTTON_5, 0,0,0,1},
	{PIN_BUTTON_6, 0,0,0,1},
	{PIN_BUTTON_7, 0,0,0,1},
	{PIN_BUTTON_8, 0,0,0,1},
	{PIN_UP,       0,0,0,1},
	{PIN_DOWN,     0,0,0,1},
	{PIN_LEFT,     0,0,0,1},
	{PIN_RIGHT,    0,0,0,1}
};

u32 pin_mask;

#define BT_INIT_ST              0
#define BT_DISCONNECT_ACK_ST    1
#define BT_COMMAND_REQ_ST       2
#define BT_COMMAND_ACK_ST       3
#define BT_CONNECT_REQ_ST       4
#define BT_CONNECT_NACK_ST      5
#define BT_INQUIRY_REQ_ST       6
#define BT_INQUIRY_ACK_ST       7
#define BT_STORE_REMOTE_REQ_ST  8
#define BT_STORE_REMOTE_ACK_ST  9
#define BT_CONNECTED_ST        10

volatile u8 bt_state;
volatile u8 bt_automatic_reconnect = 1;
volatile u8 bt_delay_ctr;

volatile u8 voltage_low = 0;   // battery warning
volatile u8 voltage_high = 0;  // connected to USB voltage

// 0xFD|6|X1|Y1|X2|Y2|Buttons0..7|Buttons8..15

#define BT_AXIS_X1 2
#define BT_AXIS_Y1 3
#define BT_AXIS_X2 4
#define BT_AXIS_Y2 5
#define BT_BUTTON0 6
#define BT_BUTTON1 7

u8 bt_data[8];
u8 bt_data_old[8];

char str_buffer[64];
char str_buffer_bgnd[64];

volatile u32 ms_counter;
volatile u32 counter_10ms;
volatile u16 v_in, v_in_filtered;
volatile u16 frame_nr_old;
volatile u8 usb_connected;


/** UART receive buffer */
char uart_rx_buf[64];
/** UART number of received bytes */
volatile u16 uart_rx_len;

char rtt_rx_buf[64];

u32 random() {
	return SCT_GetCounter(LPC_SCT0) ^ PRC_ReverseByteOrder(SCT_GetCounter(LPC_SCT1));
}

u8 pin_level_raw(u8 ch) {
	pin_debounce_t *p = &pins[ch];
	return PIN_ChGet(p->pin) ^ p->inact_lvl;
}

u8 pin_debounce(u8 ch) {
	u8 changed = 0;
	pin_debounce_t *p = &pins[ch];
	u8 lvl = PIN_ChGet(p->pin) ^ p->inact_lvl;
	if (p->state == 0) {
		// not yet debouncing
		if (lvl != p->lvl) {
			// state change -> start debouncing
			p->ctr = PIN_DEBOUNCE_COUNT;
			p->state = 1;
		}
	} else {
		// already debouncing
		if (lvl != p->lvl) {
			// current level is different than the debounced level
			if (--p->ctr <= 0) {
				// new level was debounced
				p->state = 0;
				p->lvl = lvl;
				changed = 1;
			}
		} else {
			// current level is the same as the debounced level
			if (++p->ctr >= PIN_DEBOUNCE_COUNT) {
				// reset debounce state
				p->state = 0;
			}
		}
	}
	return changed;
}

u8 pin_state(u8 ch) {
	return pins[ch].lvl;
}


/** Convert integer to string.
	@param z integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2str( int z, char* buffer ) {
	int i = 0;
	int j;
	char tmp;
	unsigned u;

	if( z < 0 ) {
		buffer[0] = '-';
		buffer++;
		u = -z;
	} else
		u = (unsigned)z;
	do {
		buffer[i++] = '0' + u % 10;
		u /= 10;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

/** Convert integer to string.
	@param u unsigned integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2hex( u32 u, char* buffer ) {
	static const char hexchar[] = "0123456789abcdef";
	int i = 0;
	int j;
	char tmp;

	do {
		buffer[i++] = hexchar[u % 16];
		u /= 16;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

char* timestring(u32 t, char *b) {
	int h,m,s,pos;
	h   = t / 3600;
	m = (t / 60) % 60;
	s = t % 60;
	// hour
	if (h<10) {
		b[0] = '0';
		pos = 1;
	} else pos = 0;
	int2str(h,&b[pos]);
	pos = strlen(b);
	b[pos++]= ':';
	// minute
	if (m<10) {
		b[pos++] = '0';
	}
	int2str(m,&b[pos]);
	pos = strlen(b);
	b[pos++]= '.';
	// second
	if (s<10) {
		b[pos++] = '0';
	}
	int2str(s,&b[pos]);
	return b;
}

/** Append space to the end of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_right(char* s, int l) {
	int len = strlen(s);
	while (len<l) {
		s[len]=' ';
		s[len+1]=0;
		len = strlen(s);
	}
}

/** Append space to the start of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_left(char* s, int l) {
	int i;
	int len = strlen(s);

	if (l > len) {
		// shift string right
		for (i=0; i<=len; i++) {
			s[l-i] = s[len-i];
		}
		// fill start with spaces
		for (i=0; i<l-len; i++)
			s[i] = ' ';
	}
}

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;
	int i;
	u32 mask=1;
	for (i=sizeof(pins)/sizeof(pins[0])-1; i>=0; i--) {
		pin_debounce(i);
		mask <<= 1;
		mask |= pin_state(i);
	}
	pin_mask = mask;

	// 12bit ADC, 1:1 voltage divider
	// Physical value = v_in * 3.3V * 2 / (1<<12)
	v_in = ADC_GetResult(0,ADC_CH_VIN);
	v_in_filtered = (u16)((63*(u32)v_in_filtered + v_in)/64);
	voltage_low = (v_in_filtered <= VOLTAGE_LOW);
	voltage_high = (v_in_filtered >= VOLTAGE_HIGH);
	PIN_ChSet(PIN_LED_BAT, !voltage_low); // low active
	adc_start_conversion(0,ADC_SEQA, ADC0_CHANNELS);
}

/*__INLINE*/ u8 convert_axis_voltage(u16 value) {
	return (value>>4)-128;
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	int i;
	u8 cnf;
	s32 temp;
	u8 buttons;
	u8 axis[NUMBER_OF_AXES];

	for (i=0; i<NUMBER_OF_AXES;i++) {
		cnf = config.axis[i].mode;
		if (cnf & CNF_ENABLE) {
			if ( cnf & CNF_DIGITAL )
				temp = 127 + pin_state(config.axis[i].resourceL)*-127 + pin_state(config.axis[i].resourceR)*127;
			else
				temp = convert_axis_voltage(axes_filtered[i]);
			if (cnf & CNF_INVERT)
				temp = 255 - temp;
		} else
			temp = 0;
		axis[i] = (u8)(temp-127);
		hid_data[i] = (u8)temp;
	}
	buttons = 0;
	for (i=NUMBER_OF_BUTTONS-1; i>=0 ;i--) {
		cnf = config.button[i].mode;
		buttons <<= 1;
		if (cnf & CNF_ENABLE) {
			buttons |= pin_state(config.button[i].resource);
		}
	}
	hid_data[2] = buttons;

	// bt
	bt_data[BT_AXIS_X1] = axis[0];
	bt_data[BT_AXIS_Y1] = axis[1];
	bt_data[BT_AXIS_X2] = axis[0];
	bt_data[BT_AXIS_Y2] = axis[1];
	bt_data[BT_BUTTON0] = buttons;
	//
	// only send BT UART message if connected and only every 2nd 10ms event to not flood the module
	if ((counter_10ms & 1) == 0)
		if (bt_state == BT_CONNECTED_ST) {
			// only transfer data if changed
			temp = 0;
			for (i=0; i<sizeof(bt_data);i++) {
				if (bt_data[i] != bt_data_old[i])
					temp = 1;
				bt_data_old[i] = bt_data[i];
			}
			if (temp != 0)
				UART_Transmit(UART_BT, bt_data, sizeof(bt_data));
		}
	counter_10ms++;

}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
	// voltage divider is 1/2, multiply with 1000 to get mV, Vref = 3.3V -> 6.6V at input is 0xfff
	// 6600/0x1000 -> 825/512
	if (bt_delay_ctr>0)
		bt_delay_ctr--;
	switch (bt_state) {
		case BT_INIT_ST:
			if ((bt_delay_ctr == 0) || (strncmp(uart_rx_buf,"%REBOOT",8) == 0))
				bt_state = BT_DISCONNECT_ACK_ST;
			break;
		case BT_DISCONNECT_ACK_ST:
			bt_state = BT_COMMAND_REQ_ST;
			UART_Transmit(UART_BT, (u8*)"$$$", 3);
			bt_delay_ctr = 20; // retry after 2 seconds
			break;
		case BT_COMMAND_ACK_ST:
			if (bt_automatic_reconnect) {
				UART_Transmit(UART_BT, (u8*)"c\r", 2);
				bt_delay_ctr = 100; // retry after 10 seconds
				bt_state = BT_CONNECT_REQ_ST;
			}
			break;
		case BT_CONNECT_NACK_ST:
			UART_Transmit(UART_BT, (u8*)"i\r", 2);
			bt_state = BT_INQUIRY_REQ_ST;
			break;
		case BT_INQUIRY_ACK_ST:
			UART_Transmit(UART_BT, (u8*)"SR,I\r", 5);
			bt_state = BT_STORE_REMOTE_REQ_ST;
			break;
		case BT_STORE_REMOTE_ACK_ST:
			bt_state = BT_COMMAND_ACK_ST;
			break;
	}
	// check USB
	u16 frame_nr = LPC_USB->INFO & 0x7ff; // FRAME_NR
	// frame counter increasing and VBUSDEBOUNCED
	usb_connected = ( (frame_nr != frame_nr_old) && (LPC_USB->DEVCMDSTAT & (1<<28)));
	frame_nr_old = frame_nr;
}


/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	(void)dummy;
	//int2str(ms_counter/1000, str_buffer);
	//SEGGER_RTT_Write(RTT_LOG, str_buffer, strlen(str_buffer));
	//SEGGER_RTT_Write(RTT_LOG, "\r\n", 2);
	PIN_ChSet(PIN_LED_BT, !(bt_state == BT_CONNECTED_ST)); // low active

}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}

/**
 * @brief	Handle interrupt from USB
 * @return	Nothing
 */
void USB_IRQHandler(void) {
	USBD_API->hw->ISR(g_hUsb);
}

int main(void) {
	SystemInit();

	SEGGER_RTT_Init();

	adc_init();
	adc_start_calibration();

	SYSTIME_Init();                         /* init system timer */
	PIN_Init();

	DMA_Init();
	DMA_IRQEnable(1);

	// Init UART
	UART_SetBaudRate(UART_BT, 115200);
	UART_Setup(UART_BT);
	bt_state = BT_INIT_ST;
	bt_delay_ctr = 10; // wait 1000ms before sending $$$ unless module sends "%REBOOT"
	UART_Transmit(UART_BT, (u8*){0}, 1); // send disconnect in case this was a running reset

	bt_data[0] = 0xFD;
	bt_data[1] = 0x06;

	USBD_API_INIT_PARAM_T usb_param;
	USB_CORE_DESCS_T desc;
	ErrorCode_t ret = LPC_OK;
	/* initialize USBD ROM API pointer. */
	g_pUsbApi = (const USBD_API_T *) LPC_ROM_API->pUSBD;

	/* power UP USB Phy */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_USBPHY_PD);
	/* Reset USB block */
	//Chip_SYSCTL_PeriphReset(RESET_USB);

	/* initialize call back structures */
	memset((void *) &usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
	usb_param.usb_reg_base = LPC_USB0_BASE;
	/*	WORKAROUND for artf44835 ROM driver BUG:
	    Code clearing STALL bits in endpoint reset routine corrupts memory area
	    next to the endpoint control data. For example When EP0, EP1_IN, EP1_OUT,
	    EP2_IN are used we need to specify 3 here. But as a workaround for this
	    issue specify 4. So that extra EPs control structure acts as padding buffer
	    to avoid data corruption. Corruption of padding memory doesn’t affect the
	    stack/program behaviour.
	 */
	usb_param.max_num_ep = 2 + 1;
	usb_param.mem_base = USB_STACK_MEM_BASE;
	usb_param.mem_size = USB_STACK_MEM_SIZE;

	/* Set the USB descriptors */
	desc.device_desc = (uint8_t *) USB_DeviceDescriptor;
	desc.string_desc = (uint8_t *) USB_StringDescriptor;

	/* Note, to pass USBCV test full-speed only devices should have both
	 * descriptor arrays point to same location and device_qualifier set
	 * to 0.
	 */
	desc.high_speed_desc = USB_FsConfigDescriptor;
	desc.full_speed_desc = USB_FsConfigDescriptor;
	desc.device_qualifier = 0;

	while (!adc_calibration_finished(0)); // needs about 290µs
	adc_start_conversion(0, ADC_SEQA, ADC0_CHANNELS);

	//I2C_Init();

	/* USB Initialization */
	ret = USBD_API->hw->Init(&g_hUsb, &desc, &usb_param);
	if (ret == LPC_OK) {
		ret = usb_hid_init(g_hUsb, (USB_INTERFACE_DESCRIPTOR *) &USB_FsConfigDescriptor[sizeof(USB_CONFIGURATION_DESCRIPTOR)],
			&usb_param.mem_base, &usb_param.mem_size);
		if (ret == LPC_OK) {
			/*  enable USB interrupts */
			NVIC_EnableIRQ(USB0_IRQn);
			/* now connect */
			USBD_API->hw->Connect(g_hUsb, 1);
		}
	}


	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	//HID_SetFeatureReport(&config,sizeof(config_t));

	while (1) {
		u16 read;

		/* Handle HID tasks */
		HID_Tasks();
		if (HID_OutputReportReceived()) {
			PRC_Int_Disable();
			HID_GetOutputReport(hid_output_report, sizeof(hid_output_report));
			//SEGGER_RTT_Write(RTT_LOG, (char*)hid_output_report, sizeof(hid_output_report));
			//SEGGER_RTT_Write(RTT_LOG, "\r\n", 2);
			PRC_Int_Enable();
		}

		read = UART_Receive(UART_BT, (u8*)uart_rx_buf, sizeof(uart_rx_buf), '\n' );
		if (read > 0)
			SEGGER_RTT_Write(RTT_BT,uart_rx_buf,read);
		else {
			// some messages from the BT module don't have a <CR>
			// %DISCONNECT : 11
			// %REBOOT     : 7
			read = UART_GetReceiveCount(UART_BT);
			if ( (read >= 7) && UART_Peek(UART_BT,0) == '%') {
				char c= UART_Peek(UART_BT,1);
				if ( ((read == 11) && (c=='D')) || ((read == 7) && (c=='R'))) {
					read = UART_Receive(UART_BT, (u8*)uart_rx_buf, sizeof(uart_rx_buf), UART_NOSCAN );
					SEGGER_RTT_Write(RTT_BT,uart_rx_buf,read);
					SEGGER_RTT_Write(RTT_BT, "\r\n", 2);
				}
			}
		}

		// console mirroring
		if (!bt_delay_ctr && SEGGER_RTT_HasData(RTT_BT)) {
			read = SEGGER_RTT_Read(RTT_BT, rtt_rx_buf, sizeof(rtt_rx_buf));
			if (rtt_rx_buf[0]=='$') {
				UART_Transmit(UART_BT, (u8*)"$$$", 3); // command mode must not send carriage return
			} else if (rtt_rx_buf[0]==' ') {          // space -> send zero to disconnect
				bt_automatic_reconnect = 0;
				UART_Transmit(UART_BT, (u8*){0}, 1);
			} else
				UART_Transmit(UART_BT,  (u8*)rtt_rx_buf, read);
		}
		//
		switch (bt_state) {
			case BT_COMMAND_REQ_ST:
				if (strncmp(uart_rx_buf,"CMD",3) == 0)
					bt_state = BT_COMMAND_ACK_ST;
				else if (bt_delay_ctr == 0)
					bt_state = BT_DISCONNECT_ACK_ST; // retry after 2 seconds
				break;
			case BT_CONNECT_REQ_ST:
				if (strncmp(uart_rx_buf,"%CONNECT",8) == 0)
					bt_state = BT_CONNECTED_ST;
				else if (strncmp(uart_rx_buf,"No Remote Address!",18) == 0)
					bt_state = BT_CONNECT_NACK_ST;
				else if (bt_delay_ctr == 0)
					bt_state = BT_COMMAND_ACK_ST;
				break;
			case BT_CONNECTED_ST:
				if (strncmp(uart_rx_buf,"%DISCONNECT",11) == 0 )
					bt_state = BT_DISCONNECT_ACK_ST;
				break;
			case BT_INQUIRY_REQ_ST:
				if ( strncmp(uart_rx_buf,"Inquiry Done",12) == 0 )
					bt_state = BT_INQUIRY_ACK_ST;
				break;
			case BT_STORE_REMOTE_REQ_ST:
				if ( strncmp(uart_rx_buf,"AOK",3) == 0 )
					bt_state = BT_STORE_REMOTE_ACK_ST;
				break;
		}

		/* Sleep until next IRQ happens */
		__WFI();
	}
}

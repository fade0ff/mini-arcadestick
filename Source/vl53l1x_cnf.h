/** VL53L1X Time of Flight Sensor Library
	Communication through I2C mode
	LPC15xx ARM Cortex M3
	--------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef VL53L1X_CNF_H
#define VL53L1X_CNF_H

#define VL53L1X_I2C_CH          1     ///! I2C channel used for communication

#endif

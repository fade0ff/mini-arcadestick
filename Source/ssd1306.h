/** SSD1306 OLED Library
	Communication through I2C mode
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SSD1306_H
#define SSD1306_H

#include "ssd1306_cnf.h"
#include "fonts.h"

#define SSD1306_I2C_ADDRESS_LO        0x3c
#define SSD1306_I2C_ADDRESS_HI        0x3d

// Fundamental Command Table
#define SSD1306_SET_CONTRAST          0x81            ///! Set contrast. One parameter byte [0..255].
#define SSD1306_ENTIRE_DISP_ON(x)     (0xA4|((x)&1))  ///! x=0: output uses RAM content, x=1: output ignores RAM content. No additional parameters.
#define SSD1306_INVERT_DISPLAY(x)     (0xA6|((x)&1))  ///! x=0: non-inverted display, x=1: inverted display. No additional parameters.
#define SSD1306_DISPLAY_ON(x)         (0xAE|((x)&1))  ///! x=0: display off (sleep), x=1: display on. No additional parameters.

//  Scrolling Command Table
#define SSD1306_SCROLL_H(x)           (0x26|((x)&1))  ///! x=0: scroll right, x=1: scroll left. 6 parameter bytes.
#define SSD1306_SCROLL_HV(x)          (0x29|((x)&3))  ///! x=1: scroll vertical and right, x=2: scroll vertical and left. 5 parameter bytes.
#define SSD1306_SCROLL_STOP           0x2E            ///! Stop scrolling. No additional parameters.
#define SSD1306_SCROLL_START          0x2F            ///! Start scrolling. No additional parameters.
#define SSD1306_SCROLL_V_AREA         0xA3            ///! Set vertical scroll area. 2 parameter bytes.

// Addressing Setting Command Table
#define SSD1306_LOWER_COLUMN(x)       (0x00|((x)&15)) ///! Set the lower nibble of the column start address register for Page Addressing Mode
#define SSD1306_HIGHER_COLUMN(x)      (0x10|((x)&15)) ///! Set the lower nibble of the column start address register for Page Addressing Mode
#define SSD1306_MEMORY_ADR_MODE       0x20            ///! Set Memory Addressing Mode. One parameter byte.
#define SSD1306_COLUMN_AREA           0x21            ///! Setup column start and end address. 2 parameter bytes.
#define SSD1306_PAGE_AREA             0x22            ///! Setup page start and end address. 2 parameter bytes.
#define SSD1306_PAGE_START(x)         (0xB0|((x)&7))  ///! Set start page for Page Addressing Mode. No additional parameters.

// Hardware Configuration (Panel resolution & layout related) Command Table
#define SSD1306_SET_START_LINE(x)     (0x40|((x)&63)) ///! Set Display Start Line (x= 0..63)
#define SSD1306_SEGMENT_REMAP(x)      (0xA0|((x)&1))  ///! x=0: column address 0 is mapped to SEG0, x=1: column address 127 is mapped to SEG0
#define SSD1306_SET_MULTIPLEX         0xA8            ///! Set Multiplex Ratio. One parameter byte.
#define SSD1306_COM_SCAN_DIR(x)       (0xC0|((x)&8))  ///! Set COM output scan direction. x=0: COM0 to COM[N –1], x=8: COM[N-1] to COM0
#define SSD1306_SET_DISPLAY_OFS       0xD3            ///! Set Display Offset. One parameter byte.
#define SSD1306_SETCOMPINS            0xDA            ///! Set COm pins HW config. One parameter byte.

// Timing & Driving Scheme Setting Command Table
#define SSD1306_DISPLAY_CLOCK_DIV     0xD5            ///! Set Display Clock Divide Ratio. One parameter byte.
#define SSD1306_VCOMH_DESELECT_LVL    0xDB            ///! Set VCOMH Deselect Level. One parameter byte.
#define SSD1306_SET_PRECHARGE         0xD9            ///! Set Precharge Period. One parameter byte.

// Charge Bump Setting Command Table
#define SSD1306_CHARGEPUMP            0x8D            ///! Charge Pump Setting. One parameter byte.


extern void SSD1306_WriteCommand(u8 cmd);
extern void SSD1306_WriteCommands(u8 *commands, u16 len);

extern void SSD1306_SetArea(u8 c1, u8 p1 , u8 c2, u8 p2);
extern void SSD1306_SetStart(u8 c,u8 p);
extern void SSD1306_SetFullScreen(void);
extern void SSD1306_DrawPixel(u8 x, u8 y, u8 set);
extern void SSD1306_FillRect(u8 c1, u8 p1, u8 c2, u8 p2, u8 set);
extern void SSD1306_FillScreen(u8 set);
extern void SSD1306_BlitRect(u8 x1, u8 y1, u8 x2, u8 y2, u8 *data);
extern void SSD1306_DrawStr(u8 x, u8 y, char* str, font_t *font, u8 set);
extern void SSD1306_UpdateRect(u8 c1, u8 p1, u8 c2, u8 p2);
extern void SSD1306_UpdateScreen(void);
extern void SSD1306_Init(void);

#endif

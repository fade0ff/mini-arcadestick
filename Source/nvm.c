/** NVM library
	Non volatile memory implemented via internal EEPROM
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "iap.h"
#include "nvm.h"
#include "prc.h"
#include "systime.h"

static const IAP iap_entry = (IAP) IAP_LOCATION;

/** NVRAM intialization - currently an empty placeholder */
void NVM_Init(void) {
	// dummy
}

/** Wait for the current NVRAM operation to be completed (poll I2C EEPROM acknowledge) */
void NVM_WaitForCompletion(void) {
	// dummy
}

/** Write data at a given address.
	@param adr    start address  in NVRAM
	@param data   pointer to buffer which contains data to be written
	@param len    number of bytes to be written
	@return success
 */
u8 NVM_Write(u16 adr, u8 *data, u8 len) {
	u32 command[5], result[4];

	command[0] = IAP_CMD_EEPROM_WRITE;
	command[1] = adr;
	command[2] = (u32)data;
	command[3] = len;
	command[4] = SYS_FRQ_CORE/1000;

	PRC_Int_Disable();
	iap_entry(command, result);
	PRC_Int_Enable();

	return result[0] == 0;
}

/** Read data from a given address
	@param adr    start address in NVRAM
	@param data   pointer to buffer where read data is stored
	@param len    number of bytes to be read
	@return success
 */
u8 NVM_Read(u16 adr, u8 *data, u8 len) {
	u32 command[5], result[4];

	command[0] = IAP_CMD_EEPROM_READ;
	command[1] = adr;
	command[2] = (u32)data;
	command[3] = len;
	command[4] = SYS_FRQ_CORE/1000;

	PRC_Int_Disable();
	iap_entry(command, result);
	PRC_Int_Enable();

	return result[0] == 0;
}

/** Synchronous Serial Library
	Communication via the SPI peripheral (i.e. SPI)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SPI_H
#define SPI_H

#include "spi_cnf.h"
#include "dma.h"

#define SPI_NUM_PHYS_DEVICES  2                        ///! Number of physical devices

#define LPC_SPI_TypeDef LPC_SPI_T

/* CFG - SPI configuration register */

/* SPI CFG Register BitMask */
#define SPI_CFG_ENABLE         (1UL << 0)              ///! enable device
#define SPI_CFG_SLAVE          0                       ///! slave mode select
#define SPI_CFG_MASTER         (1UL << 2)              ///! master mode select
#define SPI_CFG_MSB_FIRST      0                       ///! data is transmitted and received MSB first
#define SPI_CFG_LSB_FIRST      (1UL << 3)              ///! data is transmitted and received LSB first
#define SPI_CFG_CPHA0          0                       ///! clock phase 0: capture data on the first/active edge, change data on the following edge
#define SPI_CFG_CPHA1          (1UL << 4)              ///! clock phase 1: change data on the first/active edge, capture data on the following edge
#define SPI_CFG_CPOL_LO        0                       ///! clock polarity 0: passive state of the clock (between frames) is low. First/active edge is rising edge
#define SPI_CFG_CPOL_HI        (1UL << 5)              ///! clock polarity 1: passive state of the clock (between frames) is high. First/active edge is rising edge
#define SPI_CFG_LBM_EN         (1UL << 7)              ///! loop back mode enable (master mode only)
#define SPI_CFG_SPOL0_LO       0                       ///! SSEL0 is active low
#define SPI_CFG_SPOL0_HI       (1UL << 8)              ///! SSEL0 is active high
#define SPI_CFG_SPOL1_LO       0                       ///! SSEL1 is active low
#define SPI_CFG_SPOL1_HI       (1UL << 9)              ///! SSEL1 is active high
#define SPI_CFG_SPOL2_LO       0                       ///! SSEL2 is active low (SPI0 only)
#define SPI_CFG_SPOL2_HI       (1UL << 10)             ///! SSEL2 is active high (SPI0 only)
#define SPI_CFG_SPOL3_LO       0                       ///! SSEL3 is active low (SPI0 only)
#define SPI_CFG_SPOL3_HI       (1UL << 11)	           ///! SSEL3 is active high (SPI0 only)

/** Get SPI config register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return config register CFG
 */
#define SPI_GetConfig(x)       ((x)->CFG)

/** Set SPI config register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y value to write to config register (construct from SPI_CFG_xxx macros)
 */
#define SPI_SetConfig(x,y)     { (x)->CFG = (y) & 0xfbd; }


/* SPI Delay register */
#define  SPI_DLY_PRE_DELAY(n)      ((n) & 0x0F)         ///! ticks between SSEL assertion and the beginning of a data frame
#define  SPI_DLY_POST_DELAY(n)     (((n) & 0x0F) << 4)  ///! ticks between the end of a data frame and SSEL deassertion
#define  SPI_DLY_FRAME_DELAY(n)    (((n) & 0x0F) << 8)  ///! minimum amount of time between adjacent data frames
#define  SPI_DLY_TRANSFER_DELAY(n) (((n) & 0x0F) << 12) ///! Controls the minimum amount of time that the SSEL is deasserted between transfers


/** Get SPI delay register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return delay register DLY
 */
#define SPI_GetDelay(x)      ((x)->DLY)

/** Set SPI delay register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y value to write to delay register (construct from SPI_DLY_xxx macros)
 */
#define SPI_SetDelay(x,y)    { (x)->DLY = (y) & 0xffff; }


/* SPI Status register */
#define SPI_STAT_RXRDY         (1UL << 0)              ///! rx data is ready for read
#define SPI_STAT_TXRDY         (1UL << 1)              ///! tx data can be written to transmit buffer
#define SPI_STAT_RXOV          (1UL << 2)              ///! rx overflow (write 1 to clear)
#define SPI_STAT_TXUR          (1UL << 3)              ///! tx underrun (write 1 to clear, slave mode only)
#define SPI_STAT_SSA           (1UL << 4)              ///! SSEL transition from deasserted to asserted  (write 1 to clear)
#define SPI_STAT_SSD           (1UL << 5)              ///! There is SSEL transition from asserted to deasserted  (write 1 to clear)
#define SPI_STAT_STALLED       (1UL << 6)              ///! SPI in stall condition
#define SPI_STAT_ENDTRANSFER   (1UL << 7)              ///! current frame is the last frame of the current  transfer (write 1 to force eot)
#define SPI_STAT_MSTIDLE       (1UL << 8)              ///! SPI master is fully idle

/** Get SPI delay register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return delay register DLY
 */
#define SPI_GetStatus(x)      ((x)->STAT)

/** Set SPI delay register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y value to write to delay register (construct from SPI_STAT_xxx macros)
 */
#define SPI_SetStatus(x,y)    { (x)->STAT = (y) & 0x1ff; }


/* SPI Interrupt enable read and set register */

#define SPI_INT_RXRDY          (1UL << 0)              ///! rx data ready interrupt
#define SPI_INT_TXRDY          (1UL << 1)              ///! tx ready interrupt
#define SPI_INT_RXOV           (1UL << 2)              ///! rx overflow interrupt
#define SPI_INT_TXUR           (1UL << 3)              ///! tx underrun interrupt (slave mode only)
#define SPI_INT_SSA            (1UL << 4)              ///! slave select asserted interrupt
#define SPI_INT_SSD            (1UL << 5)              ///! slave select deasserted interrupt

/** Get SPI interrupt mask set/read register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return interrupt enable register INTENSET
 */
#define SPI_GetIntEnable(x)   ( (x)->INTENSET )

/** Set SPI interrupt mask set/read register.
    Write ones to enable a certain interrupt.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y value to write to interrupt enable register (construct from SPI_INT_xxx macros)
 */
#define SPI_SetIntEnable(x,y) { (x)->INTENSET = (y) & 0x3f;}


/* SPI Interrupt enable clear register */

/** Set SPI interrupt clear register.
    Write ones to disable a certain interrupt.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y value to write to interrupt enable register (construct from SPI_INT_xxx macros)
 */
#define SPI_ClrIntEnable(x,y) { (x)->INTENCLR = (y) & 0x3f;}


/* SPI Receiver Data register */

#define SPI_RXDAT_RXSSEL0_ACTIVE    0           ///! SSEL0 is in active state
#define SPI_RXDAT_RXSSEL0_INACTIVE  (1UL << 16) ///! SSEL0 is in inactive state
#define SPI_RXDAT_RXSSEL0_MASK      (1UL << 16) ///! SSEL0 rx mask
#define SPI_RXDAT_RXSSEL1_ACTIVE    0           ///! SSEL1 is in active state
#define SPI_RXDAT_RXSSEL1_INACTIVE  (1UL << 17) ///! SSEL1 is in inactive state
#define SPI_RXDAT_RXSSEL1_MASK      (1UL << 17) ///! SSEL1 rx mask
#define SPI_RXDAT_RXSSEL2_ACTIVE    0           ///! SSEL2 is in active state
#define SPI_RXDAT_RXSSEL2_INACTIVE  (1UL << 18) ///! SSEL2 is in inactive state
#define SPI_RXDAT_RXSSEL2_MASK      (1UL << 18) ///! SSEL2 rx mask
#define SPI_RXDAT_RXSSEL3_ACTIVE    0           ///! SSEL3 is in active state
#define SPI_RXDAT_RXSSEL3_INACTIVE  (1UL << 19) ///! SSEL3 is in inactive state
#define SPI_RXDAT_RXSSEL3_MASK      (1UL << 19) ///! SSEL3 rx mask
#define SPI_RXDAT_SOT               (1UL << 20) ///! start of transfer

/** Read SPI data from data register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return data value read from data register (16bit)
 */
#define SPI_GetData(x)          ((u16)((x)->RXDAT))

/** Read SPI data and status information from data register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return data value and status bits read from data register (32bit)
 */
#define SPI_GetDataStatus(x)    ((x)->RXDAT)

/** Get SPI data receive register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return data register DR
 */
#define SPI_RxRegister(x)           ((x)->RXDAT)

#define SPI_TXCTL_SSEL0_ASSERT       (0UL << 16)   ///! SSEL0 asserted
#define SPI_TXCTL_SSEL0_DEASSERT     (1UL << 16)   ///! SSEL0 deasserted
#define SPI_TXCTL_SSEL1_ASSERT       0             ///! SSEL1 asserted
#define SPI_TXCTL_SSEL1_DEASSERT     (1UL << 17)   ///! SSEL1 deasserted
#define SPI_TXCTL_SSEL2_ASSERT       (0UL << 18)   ///! SSEL2 asserted   (SPI0 only)
#define SPI_TXCTL_SSEL2_DEASSERT     (1UL << 18)   ///! SSEL2 deasserted (SPI0 only)
#define SPI_TXCTL_SSEL3_ASSERT       0             ///! SSEL3 asserted   (SPI0 only)
#define SPI_TXCTL_SSEL3_DEASSERT     (1UL << 19)   ///! SSEL3 deasserted (SPI0 only)
#define SPI_TXCTL_SSEL_MASK          0x0f0000      ///! mask for SSEL de/assertion bits
#define SPI_TXCTL_EOT                (1UL << 20)   ///! end of transfer, apply transfer_delay
#define SPI_TXCTL_EOF                (1UL << 21)   ///! end of frame, apply frame_delay
#define SPI_TXCTL_RXIGNORE           (1UL << 22)   ///! ignore RX data
#define SPI_TXCTL_LEN(n)             ((((n)-1) & 0x0f) << 24) ///! frame length in bits (1..16)

/** Get SPI data and control register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return data and control register TXDATCTL
 */
#define SPI_GetTxDataCtlRegister(x)       ((x)->TXDATCTL)

/** Write SPI data and control bits to data transmit register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param d 16bit data value and control bits to write to data register (construct from SPI_TXCTL_xxx macros)
 */
#define SPI_SetDataCtl(x, d)       {(x)->TXDATCTL = (d)&0xf7fffff;}

/** Write SPI data to data transmit register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param d 16bit data value
 */
#define SPI_SetData(x, d)          {(x)->TXDAT = (u32)((d)&0xffff);}

/** Write control bits to transmit control register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param c control bits to write to data register (construct from SPI_TXCTL_xxx macros)
 */
#define SPI_SetControl(x, c)       {(x)->TXCTRL = (c)&0xf7fffff;}

/** Get SPI transmit control register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return control register TXCTRL
 */
#define SPI_GetTxCtlRegister(x)       ((x)->TXCTRL)

/** Get SPI data transmit register.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return data register TXDAT
 */
#define SPI_GetTxRegister(x)          ((x)->TXDAT)


/* SPI Divider register */

/** Get SPI divider register.
	@param x pointer to SPI peripheral
	@return interrupt enable register DIV
 */
#define SPI_GetDivider(x)   ( (x)->DIV )

/** Set SPI Clock Divider Register
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@param y divider value 0..0xffff (actual value -1)
 */
#define SPI_SetDivider(x,y) { (x)->DIV = (u32)((y)&0xffff);}

/** Get SPI interrupt identifier.
	@param x pointer to SPI peripheral [LPC_SPI0/LPC_SPI1]
	@return interrupt number
 */
#define SPI_GetInterrupt(x) ( ((x) == LPC_SPI0) ? SPI0_IRQn : SPI1_IRQn )


/* SPI Interrupt Status register */

/* SPI INTSTAT Register Bitmask */
#define SPI_INTSTAT_BITMASK    (0x3F)
#define SPI_INTSTAT_RXRDY           (1UL << 0)               ///! rx data ready
#define SPI_INTSTAT_TXRDY           (1UL << 1)               ///! tx ready
#define SPI_INTSTAT_RXOV            (1UL << 2)               ///! rx overrun
#define SPI_INTSTAT_TXUR            (1UL << 3)               ///! tx underrun (slave mode only)
#define SPI_INTSTAT_SSA             (1UL << 4)               ///! SSEL asserted
#define SPI_INTSTAT_SSD             (1UL << 5)               ///! SSEL deasserted

/** Get SPI interrupt status register.
	@param x pointer to SSP peripheral [LPC_SSP0/LPC_SSP1]
	@return interrupt status register INTSTAT (see SSP_INT_xxx macros)
 */
#define SPI_GetIntStatus(x)   ( (x)->INTSTAT)


/* Chip select definitions */
#define SPI_CS_HW                    0xffff                  ///! marker for invalid CS port -> use HW CS instead
#define SPI_CS_ACTIVE                0                       ///! pin level for active phase (low)
#define SPI_CS_INACTIVE              1                       ///! pin level for active phase (high)

/* Chip select hold/release definitions */
#define SPI_CS_HOLD                  0                       ///! hold CS after transmission
#define SPI_CS_RELEASE               1                       ///! release CS after transmission

/* Transfer result values */
#define SPI_TRANSFER_FAILED          0                       ///! SPI transfer failed
#define SPI_TRANSFER_OK              1                       ///! SPI transfer was successfully finished
#define SPI_TRANSFER_QUEUED          2                       ///! SPI transfer was queued

/* Transfer states */
#define SPI_STATE_INACTIVE           0                       ///! There is currently no SPI transmission
#define SPI_STATE_ACTIVE             1                       ///! There is currently an SPI transmission

/* Type definitions */

/** Function pointer definition for end of transfer callback */
typedef void (*spi_callback_t)(void);

/** SPI device RAM structure */
typedef struct {
	u8  state;               ///! Current transfer state (see SPI_STATE_xxx)
	u8  active_ch;           ///! Channel currently served
	u8  queue_bottom_idx;    ///! Index to bottom of queue -> currently served queue entry
	u8  queue_entries;       ///! Number of entries currently in queue
} spi_device_t;

/** SPI channel RAM structure */
typedef struct {
#if SPI_USE_DMA
	dma_entry_t dma_link __attribute__ ((aligned(16)));  ///! Link structure for TXCTL/EOT
#endif
	u32 ctldat;              ///! store control (and data) to be able to update it e.g. for EOT
#if SPI_USE_DMA == 0
	u16 com_idx;             ///! index for communication
	u16 rx_idx;              ///! index for reception
	u16 tx_idx;              ///! index for transmission
#endif
	u8  div;                 ///! Divider value
	u8  pending_count;       ///! number of pending transfers for this channel
	u8  bytesize;            ///! 1 for up to 8bit transfers, else 2 bytes
} spi_channel_t;

/** SPI queue RAM structure */
typedef struct {
	spi_callback_t callback_start; ///! pointer to callback function before transfer is started
	spi_callback_t callback_end;   ///! pointer to callback function if transfer is finished
	u8* tx_buf;              ///! pointer to transmit buffer
	u8* rx_buf;              ///! pointer to receive buffer
	u16 len;                 ///! length of data transfer in bytes
	u8  ch_id;               ///! Reference to configuration channel
	u8  rx_inc;              ///! number of bytes to increment rx buffer
	u8  tx_inc;              ///! number of bytes to increment tx buffer
	u8  release_cs;          ///! release CS after transfer (SPI_CS_HOLD / SPI_CS_RELEASE)
} spi_queue_t;

/** SPI device configuration structure */
typedef struct {
	LPC_SPI_TypeDef* device; ///! Pointer to physical device
} spi_config_t;

/** SPI channel configuration structure */
typedef struct {
	u8  device_id;           ///! virtual device index
	u32 frequency;           ///! clock frequency
	u16 config;              ///! config: CR0: clock polarity, clock phase, Bit order
	u16 ctldat;              ///! tx control: cs, len
	u16 delay;               ///! delay configs
	u16 cs_pin_ch;           ///! chip select pin
} spi_channel_cfg_t;


/* Exported declarations */
extern const spi_config_t spi_device_cfg[SPI_VIRTUAL_DEVICE_NUM];
extern const spi_channel_cfg_t spi_channel_cfg[SPI_CHANNEL_CONFIG_NUM];

/* Prototypes */
extern void SPI_Init(void);
extern u8 SPI_StartTransferX(u8 ch_id, u8* tx_buf, u8* rx_buf, u16 len, u8 inc_tx, u8 inc_rx, u8 release_cs, spi_callback_t callback_start, spi_callback_t callback_end);
extern u8 SPI_StartTransfer(u8 ch_id, u8* tx_buf, u8* rx_buf, u16 len, spi_callback_t callback_end);
extern u8 SPI_GetDeviceID(u8 ch_id);
extern LPC_SPI_TypeDef* SPI_GetDevice(u8 ch_id);
extern void SPI_Enable(u8 device_id, u8 enable);
extern u8 SPI_GetChPendingCnt(u8 ch_id);

#endif


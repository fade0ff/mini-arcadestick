/*
 * @brief HID generic example's callabck routines
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include <stdint.h>
#include <string.h>
#include "usbd_rom_api.h"
#include "hid_generic.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/**
 * @brief Structure to hold HID data
 */
typedef struct {
	USBD_HANDLE_T hUsb;	                              ///! Handle to USB stack.
	uint8_t report[HID_REPORT_SIZE];                  ///! Last report data
	uint8_t output_report[HID_OUT_REPORT_SIZE];       ///! buffer for out report
	uint8_t feature_report[HID_FEATURE_REPORT_SIZE];  ///! buffer for feature report
	uint8_t output_len;
	uint8_t feature_len;
	uint8_t tx_busy;	     ///! Flag indicating whether a report is pending in endpoint queue.
	uint8_t feature_ready;   ///! flag indicating that a report is ready to be read
	uint8_t output_ready;    ///! flag indicating that a report is ready to be read
} HID_Ctrl_T;


/** Singleton instance of HID control */
static HID_Ctrl_T g_hid;

uint8_t hid_data[HID_REPORT_SIZE];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

extern const uint8_t HID_ReportDescriptor[];
extern const uint16_t HID_ReportDescSize;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* Routine to update HID state report */
static void HID_UpdateReport(void) {
	int i;
	for (i=0; i< HID_REPORT_SIZE; i++)
		g_hid.report[i] = hid_data[i];
}

/* HID Get Report Request Callback. Called automatically on HID Get Report Request */
static ErrorCode_t HID_GetReport(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t * *pBuffer, uint16_t *plength) {
	/* ReportID = SetupPacket.wValue.WB.L; */
	switch (pSetup->wValue.WB.H) {
		case HID_REPORT_INPUT:
			HID_UpdateReport();
			*pBuffer = &g_hid.report[0];
			*plength = HID_REPORT_SIZE;
			break;

		case HID_REPORT_OUTPUT:
			*pBuffer = &g_hid.output_report[0];
			*plength = g_hid.output_len;
			break;

		case HID_REPORT_FEATURE:
			*pBuffer = &g_hid.feature_report[0];
			*plength = g_hid.feature_len;
			break;
	}
	return LPC_OK;
}

/* HID Set Report Request Callback. Called automatically on HID Set Report Request */
static ErrorCode_t HID_SetReport(USBD_HANDLE_T hHid, USB_SETUP_PACKET *pSetup, uint8_t * *pBuffer, uint16_t length) {
	/* we will reuse standard EP0Buf */
	if (length == 0) {
		return LPC_OK;
	}
	/* ReportID = SetupPacket.wValue.WB.L; */
	switch (pSetup->wValue.WB.H) {
		case HID_REPORT_INPUT:				/* Not Supported */
			return ERR_USBD_STALL;
		case HID_REPORT_FEATURE:
			memcpy(g_hid.feature_report, *pBuffer, length);
			g_hid.feature_len = length;
			g_hid.feature_ready = 1;
			break;
		case HID_REPORT_OUTPUT:
			memcpy(g_hid.output_report, *pBuffer, length);
			g_hid.output_len = length;
			g_hid.output_ready = 1;
			break;
	}
	return LPC_OK;
}

/* HID interrupt IN endpoint handler */
static ErrorCode_t HID_Ep_In_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event) {
	switch (event) {
		case USB_EVT_IN:
			/* USB_EVT_IN occurs when HW completes sending IN packet. So clear the
		    	busy flag for main loop to queue next packet.
			 */
			g_hid.tx_busy = 0;
			break;
	}
	return LPC_OK;
}


/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* HID interface init routine */
ErrorCode_t usb_hid_init(USBD_HANDLE_T hUsb, USB_INTERFACE_DESCRIPTOR *pIntfDesc, uint32_t *mem_base, uint32_t *mem_size) {
	USBD_HID_INIT_PARAM_T hid_param;
	USB_HID_REPORT_T reports_data[1];
	ErrorCode_t ret = LPC_OK;

	/* Do a quick check of if the interface descriptor passed is the right one. */
	if ((pIntfDesc == 0) || (pIntfDesc->bInterfaceClass != USB_DEVICE_CLASS_HUMAN_INTERFACE)) {
		return ERR_FAILED;
	}

	/* Init HID params */
	memset((void *) &hid_param, 0, sizeof(USBD_HID_INIT_PARAM_T));
	hid_param.max_reports = 1;
	hid_param.mem_base = *mem_base;
	hid_param.mem_size = *mem_size;
	hid_param.intf_desc = (uint8_t *) pIntfDesc;
	/* user defined functions */
	hid_param.HID_GetReport = HID_GetReport;
	hid_param.HID_SetReport = HID_SetReport;
	hid_param.HID_EpIn_Hdlr  = HID_Ep_In_Hdlr;
	//hid_param.HID_EpOut_Hdlr = HID_Ep_Out_Hdlr;
	/* Init reports_data */
	reports_data[0].len = HID_ReportDescSize;
	reports_data[0].idle_time = 0;
	reports_data[0].desc = (uint8_t *) &HID_ReportDescriptor[0];
	hid_param.report_data  = reports_data;

	ret = USBD_API->hid->init(hUsb, &hid_param);

	/* update memory variables */
	*mem_base = hid_param.mem_base;
	*mem_size = hid_param.mem_size;
	/* store stack handle for later use. */
	g_hid.hUsb = hUsb;

	return ret;
}

void HID_SetFeatureReport(uint8_t *pBuffer, uint8_t length) {
	int len = ( HID_FEATURE_REPORT_SIZE > length) ? length :  HID_FEATURE_REPORT_SIZE;
	memcpy(g_hid.feature_report, pBuffer, len);
	g_hid.feature_len = len;
}

uint8_t HID_OutputReportReceived(void) {
	return g_hid.output_ready;
}

void HID_GetOutputReport(uint8_t *pBuffer, uint8_t length) {
	int len = (  g_hid.output_len > length) ? length :  g_hid.output_len;
	memcpy(pBuffer, g_hid.output_report, len);
	g_hid.output_ready = 0;
}

/* HID tasks */
void HID_Tasks(void) {
	/* check device is configured before sending report. */
	if ( USB_IsConfigured(g_hid.hUsb)) {
		if (g_hid.tx_busy == 0) {
			/* update report based on board state */
			HID_UpdateReport();
			/* send report data */
			g_hid.tx_busy = 1;
			USBD_API->hw->WriteEP(g_hid.hUsb, HID_EP_IN, &g_hid.report[0], HID_REPORT_SIZE);
		}
	}
	else {
		/* reset busy flag if we get disconnected. */
		g_hid.tx_busy = 0;
		g_hid.output_ready = 0;
		g_hid.feature_ready = 0;
	}

}

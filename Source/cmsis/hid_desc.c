/*
 * @brief HID example USB descriptors
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "app_usbd_cfg.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

#if HID_DESCRIPTOR_CFG == HID_DESCRIPTOR_THEC64
/**
 * HID Report Descriptor
 */
const uint8_t HID_ReportDescriptor[] = {
	HID_UsagePage(HID_USAGE_PAGE_GENERIC),
	HID_Usage(HID_USAGE_GENERIC_JOYSTICK),
	HID_Collection(HID_Application),
	HID_Collection(HID_Physical),
	HID_Usage(HID_USAGE_GENERIC_X),
	HID_Usage(HID_USAGE_GENERIC_Y),
	HID_ReportSize(8),
	HID_ReportCount(2),
	HID_LogicalMin(0),
	HID_LogicalMax(255),
	HID_PhysicalMin(0),
	HID_PhysicalMax(255),
	HID_Input(HID_Data | HID_Variable | HID_Absolute),

	HID_UsagePage(HID_USAGE_PAGE_BUTTON),
	HID_UsageMin(1),
	HID_UsageMax(8),
	HID_LogicalMin(0),
	HID_LogicalMax(1),
	HID_ReportSize(1),
	HID_ReportCount(8),
	HID_Input(HID_Data | HID_Variable | HID_Absolute),

	/* output report */
	HID_Usage(HID_USAGE_PAGE_VR),            /* Usage_Page: Vendor */
	HID_LogicalMin(0),                       /* Logical Minimum: 0 */
	HID_LogicalMax(255),                     /* Logical Maximum: 255 */
	HID_ReportSize(8),                       /* Size: 8bits */
	HID_ReportCount(HID_OUT_REPORT_SIZE),    /* Count in fields */
	HID_Output(HID_Data | HID_Variable | HID_Absolute),

	/* feature report */
	HID_Usage(HID_USAGE_PAGE_VR),            /* Usage_Page: Vendor */
	HID_LogicalMin(0),                       /* Logical Minimum: 0 */
	HID_LogicalMax(255),                     /* Logical Maximum: 255 */
	HID_ReportSize(8),                       /* Size: 8bits */
	HID_ReportCount(HID_FEATURE_REPORT_SIZE),/* Count in fields */
	HID_Feature(HID_Data | HID_Variable | HID_Absolute),

	HID_EndCollection,
	HID_EndCollection,
};

/**
 * USB Standard Device Descriptor
 */
ALIGNED(4) const uint8_t USB_DeviceDescriptor[] = {
	USB_DEVICE_DESC_SIZE,			/* bLength */
	USB_DEVICE_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0110),					/* bcdUSB : 1.10*/
	0x00,							/* bDeviceClass */
	0x00,							/* bDeviceSubClass */
	0x00,							/* bDeviceProtocol */
	USB_MAX_PACKET0,				/* bMaxPacketSize0 */
	WBVAL(0x1C59),					/* idVendor */
	WBVAL(0x024),					/* idProduct */
	WBVAL(0x21),					/* bcdDevice : 1.00 */
	0x01,							/* iManufacturer */
	0x02,							/* iProduct */
	0x00,							/* iSerialNumber */
	0x01							/* bNumConfigurations */
};

/**
 * USB FSConfiguration Descriptor
 * All Descriptors (Configuration, Interface, Endpoint, Class, Vendor)
 */
ALIGNED(4) uint8_t USB_FsConfigDescriptor[] = {
	/* Configuration 1 */
	USB_CONFIGURATION_DESC_SIZE,			/* bLength */
	USB_CONFIGURATION_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(									/* wTotalLength */
		USB_CONFIGURATION_DESC_SIZE   +
		USB_INTERFACE_DESC_SIZE       +
		HID_DESC_SIZE                 +
		USB_ENDPOINT_DESC_SIZE
		),
	0x01,							/* bNumInterfaces */
	0x01,							/* bConfigurationValue */
	0x00,							/* iConfiguration */
	USB_CONFIG_BUS_POWERED,			/* bmAttributes */
	USB_CONFIG_POWER_MA(500),		/* bMaxPower */

	/* Interface 0, Alternate Setting 0, HID Class */
	USB_INTERFACE_DESC_SIZE,		/* bLength */
	USB_INTERFACE_DESCRIPTOR_TYPE,	/* bDescriptorType */
	0x00,							/* bInterfaceNumber */
	0x00,							/* bAlternateSetting */
	0x01,							/* bNumEndpoints */
	USB_DEVICE_CLASS_HUMAN_INTERFACE,	/* bInterfaceClass */
	HID_SUBCLASS_NONE,				/* bInterfaceSubClass */
	HID_PROTOCOL_NONE,				/* bInterfaceProtocol */
	0x0,							/* iInterface */
	/* HID Class Descriptor */
	/* HID_DESC_OFFSET = 0x0012 */
	HID_DESC_SIZE,					/* bLength */
	HID_HID_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0110),					/* bcdHID : 1.11*/
	0x21,							/* bCountryCode */
	0x01,							/* bNumDescriptors */
	HID_REPORT_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(sizeof(HID_ReportDescriptor)),	/* wDescriptorLength */
	/* Endpoint, HID Interrupt In */
	USB_ENDPOINT_DESC_SIZE,			/* bLength */
	USB_ENDPOINT_DESCRIPTOR_TYPE,	/* bDescriptorType */
	HID_EP_IN,						/* bEndpointAddress */
	USB_ENDPOINT_TYPE_INTERRUPT,	/* bmAttributes */
	WBVAL(0x0008),					/* wMaxPacketSize */
	HID_REPORT_INTERVAL,		    /* bInterval */
	/* Terminator */
	0								/* bLength */
};

/**
 * USB String Descriptor (optional)
 */
const uint8_t USB_StringDescriptor[] = {
	/* Index 0x00: LANGID Codes */
	0x04,							/* bLength */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0409),					/* wLANGID : US English */
	/* Index 0x01: Manufacturer */
	(19 * 2 + 2),					/* bLength (18 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	' ', 0,
	'T', 0,
	'H', 0,
	'E', 0,
	'C', 0,
	'6', 0,
	'4', 0,
	' ', 0,
	'J', 0,
	'o', 0,
	'y', 0,
	's', 0,
	't', 0,
	'i', 0,
	'c', 0,
	'k', 0,
	' ', 0,
	' ', 0,
	' ', 0,
	/* Index 0x02: Product */
	(19 * 2 + 2),					/* bLength (18 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	' ', 0,
	'T', 0,
	'H', 0,
	'E', 0,
	'C', 0,
	'6', 0,
	'4', 0,
	' ', 0,
	'J', 0,
	'o', 0,
	'y', 0,
	's', 0,
	't', 0,
	'i', 0,
	'c', 0,
	'k', 0,
	' ', 0,
	' ', 0,
	' ', 0,
	/* Index 0x03: Serial Number */
	/* NOP */
	/* Index 0x04: Interface 0, Alternate Setting 0 */
	/* NOP */
};

#else
/**
 * HID Report Descriptor
 */
const uint8_t HID_ReportDescriptor[] = {
	HID_UsagePage(HID_USAGE_PAGE_GENERIC),
	HID_Usage(HID_USAGE_GENERIC_JOYSTICK),
	HID_Collection(HID_Application),
	HID_Collection(HID_Physical),
	HID_Usage(HID_USAGE_GENERIC_X),
	HID_Usage(HID_USAGE_GENERIC_Y),
	HID_ReportSize(8),
	HID_ReportCount(2),
	HID_LogicalMin(0),
	HID_LogicalMax(255),
	HID_PhysicalMin(0),
	HID_PhysicalMax(255),
	HID_Input(HID_Data | HID_Variable | HID_Absolute),

	HID_UsagePage(HID_USAGE_PAGE_BUTTON),
	HID_UsageMin(1),
	HID_UsageMax(8),
	HID_LogicalMin(0),
	HID_LogicalMax(1),
	HID_ReportSize(1),
	HID_ReportCount(8),
	HID_Input(HID_Data | HID_Variable | HID_Absolute),

	/* output report */
	HID_Usage(HID_USAGE_PAGE_VR),            /* Usage_Page: Vendor */
	HID_LogicalMin(0),                       /* Logical Minimum: 0 */
	HID_LogicalMax(255),                     /* Logical Maximum: 255 */
	HID_ReportSize(8),                       /* Size: 8bits */
	HID_ReportCount(HID_OUT_REPORT_SIZE),    /* Count in fields */
	HID_Output(HID_Data | HID_Variable | HID_Absolute),
	/* feature report */
	HID_Usage(HID_USAGE_PAGE_VR),            /* Usage_Page: Vendor */
	HID_LogicalMin(0),                       /* Logical Minimum: 0 */
	HID_LogicalMax(255),                     /* Logical Maximum: 255 */
	HID_ReportSize(8),                       /* Size: 8bits */
	HID_ReportCount(HID_FEATURE_REPORT_SIZE),/* Count in fields */
	HID_Feature(HID_Data | HID_Variable | HID_Absolute),
	HID_EndCollection,
	HID_EndCollection,
};

/**
 * USB Standard Device Descriptor
 */
ALIGNED(4) const uint8_t USB_DeviceDescriptor[] = {
	USB_DEVICE_DESC_SIZE,			/* bLength */
	USB_DEVICE_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0110),					/* bcdUSB : 1.10*/
	0x00,							/* bDeviceClass */
	0x00,							/* bDeviceSubClass */
	0x00,							/* bDeviceProtocol */
	USB_MAX_PACKET0,				/* bMaxPacketSize0 */
	WBVAL(0x20A0),					/* idVendor */
	WBVAL(0x41B2),					/* idProduct */
	WBVAL(0x0100),					/* bcdDevice : 1.00 */
	0x01,							/* iManufacturer */
	0x02,							/* iProduct */
	0x03,							/* iSerialNumber */
	0x01							/* bNumConfigurations */
};

/**
 * USB FSConfiguration Descriptor
 * All Descriptors (Configuration, Interface, Endpoint, Class, Vendor)
 */
ALIGNED(4) uint8_t USB_FsConfigDescriptor[] = {
	/* Configuration 1 */
	USB_CONFIGURATION_DESC_SIZE,			/* bLength */
	USB_CONFIGURATION_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(									/* wTotalLength */
		USB_CONFIGURATION_DESC_SIZE   +
		USB_INTERFACE_DESC_SIZE       +
		HID_DESC_SIZE                 +
		USB_ENDPOINT_DESC_SIZE
		),
	0x01,							/* bNumInterfaces */
	0x01,							/* bConfigurationValue */
	0x00,							/* iConfiguration */
	USB_CONFIG_BUS_POWERED,			/* bmAttributes */
	USB_CONFIG_POWER_MA(500),		/* bMaxPower */

	/* Interface 0, Alternate Setting 0, HID Class */
	USB_INTERFACE_DESC_SIZE,		/* bLength */
	USB_INTERFACE_DESCRIPTOR_TYPE,	/* bDescriptorType */
	0x00,							/* bInterfaceNumber */
	0x00,							/* bAlternateSetting */
	0x01,							/* bNumEndpoints */
	USB_DEVICE_CLASS_HUMAN_INTERFACE,	/* bInterfaceClass */
	HID_SUBCLASS_NONE,				/* bInterfaceSubClass */
	HID_PROTOCOL_NONE,				/* bInterfaceProtocol */
	0x04,							/* iInterface */
	/* HID Class Descriptor */
	/* HID_DESC_OFFSET = 0x0012 */
	HID_DESC_SIZE,					/* bLength */
	HID_HID_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0110),					/* bcdHID : 1.11*/
	0x00,							/* bCountryCode */
	0x01,							/* bNumDescriptors */
	HID_REPORT_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(sizeof(HID_ReportDescriptor)),	/* wDescriptorLength */
	/* Endpoint, HID Interrupt In */
	USB_ENDPOINT_DESC_SIZE,			/* bLength */
	USB_ENDPOINT_DESCRIPTOR_TYPE,	/* bDescriptorType */
	HID_EP_IN,						/* bEndpointAddress */
	USB_ENDPOINT_TYPE_INTERRUPT,	/* bmAttributes */
	WBVAL(0x0008),					/* wMaxPacketSize */
	HID_REPORT_INTERVAL,		    /* bInterval */
	/* Terminator */
	0								/* bLength */
};

/**
 * USB String Descriptor (optional)
 */
const uint8_t USB_StringDescriptor[] = {
	/* Index 0x00: LANGID Codes */
	0x04,							/* bLength */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	WBVAL(0x0409),					/* wLANGID : US English */
	/* Index 0x01: Manufacturer */
	(10 * 2 + 2),					/* bLength (18 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	'0', 0,
	'x', 0,
	'd', 0,
	'e', 0,
	'a', 0,
	'd', 0,
	'b', 0,
	'e', 0,
	'e', 0,
	'f', 0,
	/* Index 0x02: Product */
	(7 * 2 + 2),					/* bLength (13 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	'B', 0,
	'l', 0,
	'u', 0,
	'e', 0,
	'J', 0,
	'o', 0,
	'y', 0,
	/* Index 0x03: Serial Number */
	(10 * 2 + 2),					/* bLength (13 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	'3', 0,
	'7', 0,
	'3', 0,
	'5', 0,
	'9', 0,
	'2', 0,
	'8', 0,
	'5', 0,
	'5', 0,
	'9', 0,
	/* Index 0x04: Interface 0, Alternate Setting 0 */
	(12 * 2 + 2),					/* bLength (9 Char + Type + length) */
	USB_STRING_DESCRIPTOR_TYPE,		/* bDescriptorType */
	'H', 0,
	'I', 0,
	'D', 0,
	' ', 0,
	'J', 0,
	'O', 0,
	'Y', 0,
	'S', 0,
	'T', 0,
	'I', 0,
	'C', 0,
	'K', 0,
};
#endif

const uint16_t HID_ReportDescSize = sizeof(HID_ReportDescriptor);

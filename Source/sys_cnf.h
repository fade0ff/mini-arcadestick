/** System configuration - Configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#ifndef SYS_CNF_H
#define SYS_CNF_H

/* Peripheral Enable */
#define SYSAHBCLKCTRL0_CFG ( \
		SYS_CLK_SYS_CFG(1)    | SYS_CLK_ROM_CFG(1)    | SYS_CLK_SRAM1_CFG(1) | SYS_CLK_SRAM2_CFG(1) | SYS_CLK_FLASH_CFG(0) | \
		SYS_CLK_EEPROM_CFG(1) | SYS_CLK_MUX_CFG(1)    | SYS_CLK_SWM_CFG(1)   | SYS_CLK_IOCON_CFG(1) | SYS_CLK_GPIO0_CFG(1) | \
		SYS_CLK_GPIO1_CFG(1)  | SYS_CLK_GPIO2_CFG(1)  | SYS_CLK_PINT_CFG(1)  | SYS_CLK_GINT_CFG(0)  | SYS_CLK_DMA_CFG(1)   | \
		SYS_CLK_CRC_CFG(0)    | SYS_CLK_WWDT_CFG(0)   | SYS_CLK_RTC_CFG(0)   | SYS_CLK_ADC0_CFG(1)  | SYS_CLK_ADC1_CFG(0)  | \
		SYS_CLK_DAC_CFG(0)    | SYS_CLK_ACMP_CFG(0))

#define SYSAHBCLKCTRL1_CFG ( \
		SYS_CLK_MRT_CFG(0)    | SYS_CLK_RIT_CFG(1)    | SYS_CLK_SCT0_CFG(0)  | SYS_CLK_SCT1_CFG(0)  | SYS_CLK_SCT2_CFG(0) | \
		SYS_CLK_SCT3_CFG(0)   | SYS_CLK_SCTIPU_CFG(0) | SYS_CLK_CCAN_CFG(0)  | SYS_CLK_SPI0_CFG(1)  | SYS_CLK_SPI1_CFG(0) | \
		SYS_CLK_I2C0_CFG(1)   | SYS_CLK_UART0_CFG(1)  | SYS_CLK_UART1_CFG(0) | SYS_CLK_UART2_CFG(0) | SYS_CLK_QEI_CFG(0)  | \
		SYS_CLK_USB_CFG(1))


/* Frequency configuration */
#define SYS_FRQ_CORE        72000000UL       ///! Desired core frequency 72 MHz - has to match SYS_FRQ_PLL and SYS_CLK_DIV
#define SYS_FRQ_OSC         12000000UL       ///! Oscillator/Crystal frequency
#define SYS_FRQ_SCT         72000000UL       ///! Desired SCT frequency (only used for dedicated SCT inputs)
#define SYS_FRQ_USB         48000000UL       ///! Desired USB frequency - should be always 48MHz
#define SYS_FRQ_ASYNC_ADC   72000000UL       ///! Desired asynchronous ADC frequency

/* Define source of main clock */
#define SYS_MAIN_CLKSEL_CFG SYSCTL_MAINCLKSELB_PLL_OUT

/* Use internal RC oscillator and/or external crystal (bypass not implemented) */
#define SYS_FRQ_USE_IRC     0          ///! 1: use internal oscillator, 2: don't use internal oscillator
#define SYS_FRQ_USE_SYSOSC  1          ///! 1: use main system oscillator, 2: don't use main system oscillator

/* Enable PLLs */
#define SYS_FRQ_USE_SYS_PLL 1          ///! 1: use SYS PLL, 0: don't use SYS PLL
#define SYS_FRQ_USE_USB_PLL 1          ///! 1: use USB PLL, 0: don't use USB PLL
#define SYS_FRQ_USE_SCT_PLL 1          ///! 1: use SCT PLL, 0: don't use SCT PLL

/* Setup PLL clock sources (only needed if enabled) */
#define SYS_PLL_CLKSRC_CFG             SYSCTL_PLLCLKSRC_SYSOSC
#define SYS_USBPLL_CLKSRC_CFG          SYSCTL_PLLCLKSRC_SYSOSC
#define SYS_SCTPLL_CLKSRC_CFG          SYSCTL_PLLCLKSRC_SYSOSC

/* PLL output frequency SYS_FRQ_PLL
   SYS_FRQ_PLL = SYS_FRQ_OSC*SYS_MSEL_CFG
*/
#define SYS_MSEL_CFG       6           ///! PLL multiplier [1..64]
#define SYS_PSEL_CFG       2           ///! PLL divider  [1,2,4,8]

/* The CPU core frequency is SYS_FRQ_PLL/SYS_CLK_DIV */
#define SYS_CLK_DIV        1           ///! CLock divider [1..256]

/* USB PLL output frequency should always be 48MHz (USB)
 */
#define SYS_MSEL_USB_CFG   4           ///! USB PLL multiplier [1..64]
#define SYS_PSEL_USB_CFG   2           ///! USB PLL divider [1,2,4,8]

/* SCT PLL output frequency
 */
#define SYS_MSEL_SCT_CFG   6           ///! SCT PLL multiplier [1..64]
#define SYS_PSEL_SCT_CFG   2           ///! SCT PLL divider [1,2,4,8]

/* Peripheral clock selects */
#define SYS_CLK_USB_CLKSEL_CFG         SYSCTL_USBCLKSEL_USBPLL
#define SYS_CLK_ADCASYNC_CLKSEL_CFG    SYSCTL_ADCASYNCCLKSEL_SCTPLL
#define SYS_CLK_CLKOUT_CLKSEL_CFG      SYSCTL_CLKOUTSELB_SCTPLL


/* Peripheral clock dividers 0..255 - 0 to disable */
#define SYS_CLK_SYSTICK_DIV   1
#define SYS_CLK_UART_DIV      1
#define SYS_CLK_IOCON_DIV     1
#define SYS_CLK_TRACE_DIV     1
#define SYS_CLK_USB_DIV       1
#define SYS_CLK_ADC_AYSNC_DIV 1
#define SYS_CLK_CLKOUT_DIV    1


#endif

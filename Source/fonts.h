/**
 * |----------------------------------------------------------------------
 * | Copyright (C) Tilen Majerle, 2014
 * |
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * |
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */

/**
 * Fonts library
 */
#ifndef FONTS_H
#define FONTS_H

/**
 * Font struct
 */
typedef struct {
	const u8 *data;
	u8 width;
	u8 height;
} font_t;

extern font_t font_7x10;
extern font_t font_8x8;
extern font_t font_8x12;
extern font_t font_8x16;
extern font_t font_11x18;
extern font_t font_16x26;
extern font_t font_16x16;

#endif
